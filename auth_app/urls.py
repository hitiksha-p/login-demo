from django.urls import path
from . import views

app_name = 'auth_app'
urlpatterns = [
    path('', views.index, name='index'),
    path('user_register',views.user_register, name = 'user_register'),
    path('user_login',views.user_login, name = 'user_login'),
    path('user_details',views.user_details, name = 'user_details'),
    path('user_logout',views.user_logout, name = 'user_logout')
]