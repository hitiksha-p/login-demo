from django.db import models
from django.contrib.auth.models import AbstractUser
# from . manager import UserManager

# Create your models here.

class User(AbstractUser):
    username = models.CharField(max_length=20, unique=True)

    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    email_token = models.CharField(max_length=100, blank=True, null=True)
    is_verified = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username',]

    # objects = UserManager()
