from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import *
import uuid
from django.contrib import messages
from django.contrib.auth import authenticate , login, logout
from django.http import HttpResponseRedirect


# Create your views here.

def index(request):
    return render(request, 'index.html')

def user_register(request):
	if request.method == "POST":
		form = UserRegister(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			return HttpResponseRedirect("/user_login")
	form = UserRegister()
	return render (request, "register.html", context={"form":form})


def user_login(request):
    if request.method == "POST":
        email = request.POST['username']
        password = request.POST['password']
       
        user = authenticate(request,email=email, password=password)
        if user is not None:
            login(request, user)
            return redirect('/user_details')
        else:
            return redirect('/user_login')
    else:
        return render(request, "login.html")

def user_details(request):
    return render(request, 'user_details.html')

def user_logout(request):
    logout(request)
    return redirect('index')    